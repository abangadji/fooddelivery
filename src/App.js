import React, { Component } from "react";

import Header from "./components/headers";
import Tabsy from "./components/tabs";
class App extends Component {
  state = {
    burger: [
      {
        id: 1,
        photo: "../assets/photos/burger.png",
        title: "KING BURGER",
        companyName: "burger planet",
        price: 5000,
        quantity: 1,
        isAdded: false,
        item: "burger",
      },
      {
        id: 2,
        photo: "../assets/photos/ham-burger.png",
        title: "HAM BURGER",
        companyName: "Simba",
        price: 4500,
        quantity: 1,
        isAdded: false,
        item: "burger",
      },
      {
        id: 3,
        photo: "../assets/photos/chikenburger.png",
        title: "CHIKEN BURGER",
        companyName: "Simba",
        price: 5500,
        quantity: 1,
        isAdded: false,
        item: "burger",
      },
      {
        id: 4,
        photo: "../assets/photos/chiken-burger2.png",
        title: "BEEF BURGER",
        companyName: "Simba",
        price: 5500,
        quantity: 1,
        isAdded: false,
        item: "burger",
      },
      {
        id: 5,
        photo: "../assets/photos/chiken-burger2.png",
        title: "VEGGAN BURGER",
        companyName: "Simba",
        price: 5500,
        quantity: 1,
        isAdded: false,
        item: "burger",
      },
    ],
    pizza: [
      {
        id: 1,
        photo: "../assets/photos/veggie-pizza.png",
        title: "VEGGIE PIZZA",
        companyName: "burger planet",
        price: 9000,
        quantity: 1,
        isAdded: false,
        item: "pizza",
      },
      {
        id: 2,
        photo: "../assets/photos/chicago-pizza.png",
        title: "CHICAGO PIZZA",
        companyName: "burger planet",
        price: 9500,
        quantity: 1,
        isAdded: false,
        item: "pizza",
      },
      {
        id: 3,
        photo: "../assets/photos/hawaii-pizza.png",
        title: "HAWAII PIZZA",
        companyName: "burger planet",
        price: 8000,
        quantity: 1,
        isAdded: false,
        item: "pizza",
      },
      {
        id: 4,
        photo: "../assets/photos/pizza-portugese.png",
        title: "PORTUGESE PIZZA",
        companyName: "burger planet",
        price: 11000,
        quantity: 1,
        isAdded: false,
        item: "pizza",
      },
    ],
    cakes: [
      {
        id: 1,
        photo: "../assets/photos/cupcakes1.png",
        title: "DARK CUPCAKES",
        companyName: "burger planet",
        price: 1000,
        quantity: 1,
        isAdded: false,
        item: "cakes",
      },
      {
        id: 2,
        photo: "../assets/photos/cupcakes2.png",
        title: "STRAWBERRY CAKES",
        companyName: "Simba",
        price: 800,
        quantity: 1,
        isAdded: false,
        item: "cakes",
      },
      {
        id: 3,
        photo: "../assets/photos/breakfast-muffin.png",
        title: "BREAKFAST MUFFINS",
        companyName: "Simba",
        price: 500,
        quantity: 1,
        isAdded: false,
        item: "cakes",
      },
      {
        id: 4,
        photo: "../assets/photos/cupcakes3.png",
        title: "DARK MUFFINS",
        companyName: "Simba",
        price: 500,
        quantity: 1,
        isAdded: false,
        item: "cakes",
      },
    ],
    cookies: [
      {
        id: 1,
        photo: "../assets/photos/almond-cookies.png",
        title: "ALMOND COOKIES",
        companyName: "burger planet",
        price: 200,
        quantity: 1,
        isAdded: false,
        item: "cookies",
      },
      {
        id: 2,
        photo: "../assets/photos/cookies.png",
        title: "CHOCOLATE COOKIES",
        companyName: "Simba",
        price: 250,
        quantity: 1,
        isAdded: false,
        item: "cookies",
      },
      {
        id: 3,
        photo: "../assets/photos/coconut-cookies.png",
        title: "COCONUT COOKIES",
        companyName: "Simba",
        price: 500,
        quantity: 1,
        isAdded: false,
        item: "cookies",
      },
      {
        id: 4,
        photo: "../assets/photos/choco-cookies.png",
        title: "CHOCOLAT COOKIES",
        companyName: "Simba",
        price: 300,
        quantity: 1,
        isAdded: false,
        item: "cookies",
      },
      {
        id: 5,
        photo: "../assets/photos/dark-cookies.png",
        title: "DARK COOKIES",
        companyName: "Simba",
        price: 600,
        quantity: 1,
        isAdded: false,
        item: "cookies",
      },
      {
        id: 6,
        photo: "../assets/photos/choco-chips2.png",
        title: "CHIPS COOKIES",
        companyName: "Simba",
        price: 250,
        quantity: 1,
        isAdded: false,
        item: "cookies",
      },
    ],
    temp: [],
  };
  // handle removing an item
  handleRemove = (title, item) => {
    let index1 = this.state.temp.findIndex((c) => c.title === title);
    this.state.temp.splice(index1, 1);
    switch (item) {
      case "burger":
        let removeBurger = { ...this.state };
        let indexB = removeBurger.burger.findIndex((e) => e.title === title);
        removeBurger.burger[indexB].isAdded = false;
        this.setState(removeBurger);
        break;
      case "pizza":
        let removePizza = { ...this.state };
        let indexP = removePizza.pizza.findIndex((e) => e.title === title);
        removePizza.pizza[indexP].isAdded = false;
        this.setState(removePizza);
        break;
      case "cookies":
        let removeCookies = { ...this.state };
        let indexCo = removeCookies.cookies.findIndex((e) => e.title === title);
        removeCookies.cookies[indexCo].isAdded = false;
        this.setState(removeCookies);
        break;
      case "cakes":
        let removeCakes = { ...this.state };
        let indexCa = removeCakes.cakes.findIndex((e) => e.title === title);
        removeCakes.cakes[indexCa].isAdded = false;
        this.setState(removeCakes);
        break;
      default:
        return this.state;
    }
  };

  // handle adding item to  the cart
  handleAdd = (obje) => {
    let newObj = { ...obje };
    let index = newObj.id;
    let item = newObj.item;
    this.state.temp.push(newObj);

    switch (item) {
      case "burger":
        let itemAdd = { ...this.state };
        itemAdd.burger[index - 1].isAdded = true;
        this.setState(itemAdd);
        break;
      case "pizza":
        let itemAdd2 = { ...this.state };
        itemAdd2.pizza[index - 1].isAdded = true;
        this.setState(itemAdd2);
        break;
      case "cakes":
        let itemAdd3 = { ...this.state };
        itemAdd3.cakes[index - 1].isAdded = true;
        this.setState(itemAdd3);
        break;
      case "cookies":
        let itemAdd4 = { ...this.state };
        itemAdd4.cookies[index - 1].isAdded = true;
        this.setState(itemAdd4);
        break;
      default:
        return this.state;
    }
  };
  // comments to update array item
  // one way to do this is to first extract the array index of the item you want to update
  // i.e if you're dealing with cookies then
  // grab your state and initialize it into a variable
  // const newState = this.state.foodtypes;
  // const cookieIdx = newState.cookies.findIndex(item => item.id === 1)
  // then use that index to update the object value
  // cookies[cookieIdx].quantity = some quantity
  // then simply update your state with new state

  render() {
    return (
      <React.Fragment>
        <Header
          pizza={this.state.pizza.filter((c) => c.isAdded === true).length}
          cakes={this.state.cakes.filter((c) => c.isAdded === true).length}
          cookies={this.state.cookies.filter((c) => c.isAdded === true).length}
          burger={this.state.burger.filter((c) => c.isAdded === true).length}
          foodType={this.state}
          onRemove={this.handleRemove}
        />
        <div className="to-center">
          <div className="separator"> </div>
          <div className=" container">
            <Tabsy foodtype={this.state} onAdd={this.handleAdd} />
          </div>
        </div>
        <footer class="footer mt-auto py-3">
          <div class="container to-center">
            <span class="text-muted">foodCo@manzi2020</span>
          </div>
        </footer>
      </React.Fragment>
    );
  }
}

export default App;
