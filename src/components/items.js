import React, { Component } from "react";
import Checkout from "./checkout";
class Items extends Component {
  state = {
    temp: [this.props.foodType.temp],
    total: [],
  };

  handleDecrememt = (title) => {
    let newquantity = { ...this.state };
    let r = newquantity.temp.map((e) =>
      e.filter((c) => c.title === title)
    )[0][0].quantity--;
    this.state.total.push(r * this.state.price);
    this.setState(newquantity);
  };

  handleIncrememt = (title) => {
    let newquantity = { ...this.state };
    let r = newquantity.temp.map((e) =>
      e.filter((c) => c.title === title)
    )[0][0].quantity++;
    this.state.total.push(r * this.state.price);
    this.setState(newquantity);
  };

  render() {
    return this.props.foodType.temp.length < 1 ? (
      <ul className="list-group mb-3 checkout-invisible text-center">
        <li className="list-group-item list-group-item-danger d-flex justify-content-between lh-condensed">
          <div className="col">
            <h6>Cart empty!</h6>{" "}
          </div>
        </li>
      </ul>
    ) : (
      <React.Fragment>
        <ul className="list-group mb-3 checkout-invisible">
          <li className="list-group-item list-group-item-warning d-flex justify-content-between lh-condensed">
            <div className="col">
              <h6 className="my-0">Product name</h6>
            </div>
            <div className="col">
              <h6 className="my-0">Quantity</h6>
            </div>
            <div className="col"></div>
            <span className="my-0">Amount</span>
          </li>
          {this.state.temp.map((e) =>
            e.map((description) => (
              <Checkout
                description={description}
                key={description.title}
                onIncrement={this.handleIncrememt}
                onDecrement={this.handleDecrememt}
                onRemove={this.props.onRemove}
              />
            ))
          )}
          <li className="list-group-item list-group-item-light d-flex justify-content-between">
            <span>
              <strong>total</strong>
            </span>
            <strong></strong>
          </li>
          <button className="btn btn-primary p-2 ">ORDER</button>
        </ul>
      </React.Fragment>
    );
  }
}

export default Items;
