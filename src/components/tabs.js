import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import ItemPizza from "./itemPizza";
import ItemBurger from "./itemBurger";
import ItemCakes from "./itemCakes";
import ItemCookies from "./itemCookies";
class Tabsy extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Tabs>
          <TabList>
            <Tab> Burger</Tab>
            <Tab>Pizza</Tab>
            <Tab>Cakes</Tab>
            <Tab>Cookies</Tab>
          </TabList>

          <TabPanel>
            <div className="row text-center display">
              {this.props.foodtype.burger.map((burger) => (
                <ItemBurger
                  burger={burger}
                  key={burger.id}
                  onAdd={this.props.onAdd}
                />
              ))}
            </div>
          </TabPanel>
          <TabPanel>
            <div className="row text-center display">
              {this.props.foodtype.pizza.map((pizza) => (
                <ItemPizza
                  pizza={pizza}
                  key={pizza.id}
                  onAdd={this.props.onAdd}
                />
              ))}
            </div>
          </TabPanel>
          <TabPanel>
            <div className="row text-center display">
              {this.props.foodtype.cakes.map((cakes) => (
                <ItemCakes
                  cakes={cakes}
                  key={cakes.id}
                  onAdd={this.props.onAdd}
                />
              ))}
            </div>
          </TabPanel>
          <TabPanel>
            <div className="row text-center display">
              {this.props.foodtype.cookies.map((cookies) => (
                <ItemCookies
                  cookies={cookies}
                  key={cookies.id}
                  onAdd={this.props.onAdd}
                />
              ))}
            </div>
          </TabPanel>
        </Tabs>
      </React.Fragment>
    );
  }
}

export default Tabsy;
