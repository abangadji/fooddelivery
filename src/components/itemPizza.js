import React, { Component } from "react";

class ItemPizza extends Component {
  render() {
    let classes = "btn  col-8 m-2 btn-";
    classes = this.styling(classes);
    return (
      <div className="display-card">
        <div className="display-body text-center">
          <div className="img-container">
            <img
              className=".img-fluid "
              src={this.props.pizza.photo}
              alt={this.props.pizza.title}
            />
          </div>
          <h3>{this.props.pizza.title}</h3>
          <p>{this.props.pizza.companyName}</p>
          <div className="badge badge-danger p-2">
            {`RWF ${this.props.pizza.price}`}
          </div>

          <br />
          <button
            className={classes}
            onClick={() => this.props.onAdd(this.props.pizza)}
            disabled={this.props.pizza.isAdded === true}
          >
            {this.props.pizza.isAdded === false ? "ADD TO CART" : "ADDED"}
          </button>
        </div>
      </div>
    );
  }
  styling(classes) {
    classes += this.props.pizza.isAdded === false ? "primary" : "secondary";
    return classes;
  }
}

export default ItemPizza;
