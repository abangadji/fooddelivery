import React, { Component } from "react";
import Items from "./items";
import Search from "./search";
import cart from "../cart.svg";
class Headers extends Component {
  constructor() {
    super();
    this.state = {
      isActive: false,
    };
    this.toggleOnClick = this.toggleOnClick.bind(this);
  }

  toggleMnu = () => {
    this.setState({ isActive: false });
  }; // Show menu function

  hideMnu = () => {
    this.setState({ isActive: true });
  }; // Hide menu function

  toggleOnClick = () => {
    this.setState({ isActive: !this.state.isActive });
  };

  render() {
    return (
      <React.Fragment>
        <div className="header to-center bg-white fixed-top shadow-sm">
          <nav className="navbar ">
            <a className="brand-name " href="../../index.html">
              FoodCo.
            </a>
            <Search />
            <span>
              <div className="chart" onClick={this.toggleOnClick}>
                <div className="cart-image">
                  <img alt="cart" src={cart} />
                </div>
                <div
                  className="text-center  text-white cart-number"
                  style={{
                    fontSize: "15px",
                    fontWeight: "200px",
                  }}
                >
                  {this.props.cakes +
                    this.props.pizza +
                    this.props.cookies +
                    this.props.burger}
                </div>
              </div>
            </span>
          </nav>
          {this.state.isActive ? (
            <Items
              foodType={this.props.foodType}
              onRemove={this.props.onRemove}
            />
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}

export default Headers;
