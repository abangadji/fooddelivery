import React, { Component } from "react";

class ItemCakes extends Component {
  render() {
    let classes = "btn  col-8 m-2 btn-";
    classes = this.styling(classes);
    return (
      <div className="display-card">
        <div className="display-body text-center">
          <div className="img-container">
            <img
              className=".img-fluid "
              src={this.props.cakes.photo}
              alt={this.props.cakes.title}
            />
          </div>
          <h3>{this.props.cakes.title}</h3>
          <p>{this.props.cakes.companyName}</p>
          <div className="badge badge-danger p-2">
            {`RWF ${this.props.cakes.price}`}
          </div>

          <br />
          <button
            className={classes}
            onClick={() => this.props.onAdd(this.props.cakes)}
            disabled={this.props.cakes.isAdded === true}
          >
            {this.props.cakes.isAdded === false ? "ADD TO CART" : "ADDED"}
          </button>
        </div>
      </div>
    );
  }

  styling(classes) {
    classes += this.props.cakes.isAdded === false ? "primary" : "secondary";
    return classes;
  }
}

export default ItemCakes;
