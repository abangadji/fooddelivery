import React, { Component } from "react";

class ItemBurger extends Component {
  render() {
    let classes = "btn  col-8 m-2 btn-";
    classes = this.styling(classes);
    return (
      <div className="display-card">
        <div className="display-body text-center">
          <div className="img-container">
            <img
              className=".img-fluid "
              src={this.props.burger.photo}
              alt={this.props.burger.title}
            />
          </div>
          <h3>{this.props.burger.title}</h3>
          <p>{this.props.burger.companyName}</p>
          <div className="badge badge-danger p-2">
            {`RWF ${this.props.burger.price}`}
          </div>

          <br />
          <button
            className={classes}
            onClick={() => this.props.onAdd(this.props.burger)}
            disabled={this.props.burger.isAdded === true}
          >
            {this.props.burger.isAdded === false ? "ADD TO CART" : "ADDED"}
          </button>
        </div>
      </div>
    );
  }
  styling(classes) {
    classes += this.props.burger.isAdded === false ? "primary" : "secondary";
    return classes;
  }
}

export default ItemBurger;
