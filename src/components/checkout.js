import React, { Component } from "react";
class Chekout extends Component {
  render() {
    return (
      <React.Fragment>
        <li className="list-group-item d-flex justify-content-between lh-condensed">
          <div className="col">
            <h6 className="my-0 ">{this.props.description.title}</h6>
            <span>
              <small className="text-muted">
                {this.props.description.companyName}
              </small>
            </span>
          </div>

          <div className="col">
            <span>
              <button
                disabled={this.props.description.quantity <= 1}
                className="btn btn-primary mb-1 mr-1 "
                onClick={() =>
                  this.props.onDecrement(
                    this.props.description.title,
                    this.props.description.item
                  )
                }
              >
                -
              </button>
              <div className="badge  m-0 p-0 col-2  ">
                {this.props.description.quantity}
              </div>
              <button
                className="btn btn-primary  "
                onClick={() =>
                  this.props.onIncrement(
                    this.props.description.title,
                    this.props.description.item
                  )
                }
              >
                +
              </button>
            </span>
          </div>
          <div className="col">
            <button
              className="btn btn-danger mr-2"
              onClick={() =>
                this.props.onRemove(
                  this.props.description.title,
                  this.props.description.item
                )
              }
            >
              remove
            </button>
          </div>
          <span className="text-muted">
            {this.props.description.quantity * this.props.description.price}
          </span>
        </li>
      </React.Fragment>
    );
  }
}

export default Chekout;
