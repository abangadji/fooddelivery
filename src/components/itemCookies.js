import React, { Component } from "react";

class ItemCookies extends Component {
  render() {
    let classes = "btn  col-8 m-2 btn-";
    classes = this.styling(classes);
    return (
      <div className="display-card">
        <div className="display-body text-center">
          <div className="img-container">
            <img
              className=".img-fluid "
              src={this.props.cookies.photo}
              alt={this.props.cookies.title}
            />
          </div>
          <h3>{this.props.cookies.title}</h3>
          <p>{this.props.cookies.companyName}</p>
          <div className="badge badge-danger p-2">
            {`RWF ${this.props.cookies.price}`}
          </div>

          <br />
          <button
            className={classes}
            onClick={() => this.props.onAdd(this.props.cookies)}
            disabled={this.props.cookies.isAdded === true}
          >
            {this.props.cookies.isAdded === false ? "ADD TO CART" : "ADDED"}
          </button>
        </div>
      </div>
    );
  }
  styling(classes) {
    classes += this.props.cookies.isAdded === false ? "primary" : "secondary";
    return classes;
  }
}

export default ItemCookies;
